package com.cn.liukaku.service.admintw.controller;

import com.cn.liukaku.common.domain.TbSysUser;
import com.cn.liukaku.common.dto.BaseResult;
import com.cn.liukaku.service.admintw.service.AdminService;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping(value = "v1/admins")
public class AdminController {

    @Resource
    private AdminService<TbSysUser> adminService;

    @RequestMapping(value = "page/{pageNum}/{pageSize}", method = RequestMethod.GET)
    public BaseResult page(@PathVariable(required = true) int pageNum,
                           @PathVariable(required = true) int pageSize,
                           @RequestParam(required = false) TbSysUser tbSysUser) {
        System.out.println("####################");

        PageInfo pageInfo = adminService.page(pageNum, pageSize, tbSysUser);

        List<TbSysUser> list = pageInfo.getList();

        BaseResult.Cursor cursor=new BaseResult.Cursor();

        cursor.setTotal(new Long(pageInfo.getTotal()).intValue());

        cursor.setOffset(pageInfo.getPageNum());

        cursor.setLimit(pageInfo.getPageSize());

        return BaseResult.ok(list,cursor);
    }
}
