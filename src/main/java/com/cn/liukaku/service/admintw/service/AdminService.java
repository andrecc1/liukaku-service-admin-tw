package com.cn.liukaku.service.admintw.service;

import com.cn.liukaku.common.domain.BaseDomain;
import com.cn.liukaku.common.service.BaseService;


public interface AdminService<T extends BaseDomain> extends BaseService<T> {

}
