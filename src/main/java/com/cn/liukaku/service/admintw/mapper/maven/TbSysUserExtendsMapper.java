package com.cn.liukaku.service.admintw.mapper.maven;

import com.cn.liukaku.common.domain.TbPostsPost;
import com.cn.liukaku.common.domain.TbSysUser;
import org.springframework.stereotype.Repository;
import tk.mybatis.mymapper.MyMapper;

@Repository("tbSysUserExtendsMapper")
public interface TbSysUserExtendsMapper extends MyMapper<TbSysUser> {

}
