package com.cn.liukaku.service.admintw.service.impl;


import com.cn.liukaku.common.domain.TbSysUser;
import com.cn.liukaku.common.mapper.maven.TbSysUserMapper;
import com.cn.liukaku.common.service.impl.BaseServiceImpl;
import com.cn.liukaku.service.admintw.service.AdminService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("adminServiceImpl")
@Transactional(readOnly = true)
public class AdminServiceImpl extends BaseServiceImpl<TbSysUser, TbSysUserMapper> implements AdminService<TbSysUser> {
}
