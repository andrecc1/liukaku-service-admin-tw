package com.cn.liukaku.service.admintw;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import tk.mybatis.spring.annotation.MapperScan;

@MapperScan(basePackages = {"com.cn.liukaku.service.admintw.mapper","com.cn.liukaku.common.mapper"})
@SpringBootApplication
@EnableEurekaClient
public class ServiceAdminTwApplication {
    public static void main(String[] args) {
        SpringApplication.run(ServiceAdminTwApplication.class, args);
    }
}
